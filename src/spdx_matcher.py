from nltk.tokenize import word_tokenize
import re
import json
import Levenshtein

with open('spdx_master.json', 'r') as spdx:
    spdxList = json.load(spdx)

def build_dict_name_id():
    name_id_spdx = {}
    for std in spdxList["licenses"]:
        name_id_spdx[std["name"]] = std["licenseId"]
    return(name_id_spdx)

def replace_later(name):
    r = re.match(r'^.*[>][=][ ]?\d[.]?\d?', name)
    if r:
        new_name = r.group(0) + '+'
        new_name = new_name.replace('>=', '')
        return(new_name)
    else:
        return(name)

def groom_string_id(name):
    lic = name.lower()
    lic = lic.replace('+ file license', '')
    lic = lic.replace('| file license', '')
    lic = lic.replace('license','')
    lic = lic.replace('==', '')
    lic = lic.replace(',', '-')
    lic = lic.replace('_', '-')
    lic = lic.replace('|', '-')
    lic = lic.replace('(', '')
    lic = lic.replace(')', '')
    lic = replace_later(lic) # '+'  would be equivalent
    lic = lic.replace('v','-')
    lic = lic.strip()
    
    return(lic)

def groom_string_name(name):
    lic = name.lower()
    lic = lic.replace('only', '')
    lic = lic.replace('+ file license', '')
    lic = lic.replace('| file license', '')
    lic = lic.replace('==', '')
    lic = lic.replace(',', '-')
    lic = lic.replace('_', '-')
    lic = lic.replace('+', '-')
    lic = lic.replace('|', '-')
    lic = lic.replace('(', '')
    lic = lic.replace(')', '')
    lic = replace_later(lic)
    lic = lic.strip()

    return(lic)


def find_num(s):
    st = s.replace('0','')
    st = st.replace('v',' ')
    st = st.replace('-',' ')
    re_matchData = re.compile(r'\-?\d{1,10}(?:\.\d{1,10})?')
    data = re.findall(re_matchData, st)
    if data:
        return(data[0])
    else:
        return


def best_match_number(lic, matches_dists):
    
    ## Levenshtein does not distinguish diff numbers :(
    ## so additional calculations are needed to correctly match license versions

    # if there is a number in input license, top matches with same number are prefered
    ## matches with lowest distance are considered
    min_value = min(matches_dists.values())
    best_matches = [k for k in matches_dists if matches_dists[k] == min_value]

    best_match = ''
    # if license contain number, match that contains same number is prefered
    if find_num(lic):
        for match in best_matches:
            if find_num(match):
                # if number in input license and match is same, that match is prefered
                if find_num(lic) == find_num(match):
                    if len(match)>len(best_match):
                        best_match = match
        if best_match:
            return({best_match : matches_dists[best_match]})
        else:
            #if no match has the same number, all matches with lowest distance are returned
            return(dict((k, matches_dists[k]) for k in best_matches))

    # if license does not have a number
    else:
        best_match = best_matches[0]
        # longest match is considered best match
        for match in best_matches:
            if len(match)>len(best_match):
                best_match = match
        return({best_match : matches_dists[best_match]})


def distance_matches(input_license, matches, accept_threshold = 7):
    dists = {}
    
    for match in matches:
        ## Compute the Levenshtein distance between the input license and the
        ## associated tokens. We compute it here and use it along this method
        distance = Levenshtein.distance(input_license, match)
        ratio = len(match)/2
        
        ## We keep both the distance and the ratio of distance and the length
        ## of the original token. It will be useful to prioritize the best
        ## match
        if distance < accept_threshold and distance < ratio:
            dists.setdefault(match, (distance, distance / len(input_license)))

    return(dists)


def match_token(token):
    '''
    Returns a list of matches
    '''
    token = groom_string_id(token)
    matches = set()

    for std in spdxList["licenses"]:
        std = std["licenseId"].lower()
        
        if re.search("^gpl", token):
            if re.search("^gpl", std):
                matches.add(std)

        elif re.search("^lgpl", token):
            if re.search("^lgpl", std):
                matches.add(std)

        elif re.search("^agpl", token):
            if re.search("^agpl", std):
                matches.add(std)
                
        elif token in std: # better more strict? regex from ^?
            matches.add(std)
        
        elif std in token:
            matches.add(std)
    
    if matches:
        matches_w_dist = distance_matches(token, matches)
        if matches_w_dist:
            best_match = best_match_number(token, matches_w_dist)
            return(best_match)
          
    return({})

 
def match_tokens_short_names(license_string):

    ## lowercase + tokenize license
    tokens = word_tokenize(license_string.lower())
    matches_short_names = {}

    for token in tokens:
        matches_short_names = { **matches_short_names, **match_token(token)}
    
    best_matches_short = dict((k, matches_short_names[k]) for k in matches_short_names.keys() if matches_short_names[k] == min(matches_short_names.values()))

    return(best_matches_short)


def match_main_in_name(license, accept_threshold = 11):
    
    ## Capture matches lower than a given threshold
    matches_name = {}
    
    ## Lower input license name
    input_license = license.lower()
    
    for std in spdxList["licenses"]:
        ## Lower the full name license
        long_name = std["name"].lower()
        ## ... and compute the Levenshtein distance
        distance = Levenshtein.distance(input_license, long_name)

        ## If a given full name license is lower than a predefined threshold,
        ## keep it.
        ## We keep both the distance and the ratio of distance and the length
        ## of the original token. It will be useful to prioritize the best
        ## match
        if distance < accept_threshold:
            matches_name[std["name"]] = (distance, distance/len(input_license))
    
    if matches_name:
        best_match = best_match_number(input_license, matches_name)
        return(best_match)

    ## Return all found matches
    return()


def compare_short_long(short_matches, long_matches):

    ## Load dictionary containing as key the license full name and as value,
    ## the license ID.
    name_id_spdx = build_dict_name_id()
    
    ## Map the short matches - get rid on unlicense for now
    short_names = dict((key, short_matches[key]) \
        for key in short_matches if key != 'unlicense')
    
    ## We do the same for the full license names.
    long_names = dict((name_id_spdx[key], long_matches[key]) \
        for key in long_matches)
    
    ## we look for coincidences in long and short matches
    overlapping = set(short_names.keys()) & set(long_names.keys())
    if overlapping:
        return(overlapping)

    merged_matches = { **short_names, **long_names }    
    ## Calculate the minimum distance
    min_distance = min([merged_matches[key][0] for key in merged_matches])

    ## Calculate the minimum ratio
    min_ratio = min([merged_matches[key][1] for key in merged_matches])

    ## We first filter by the best ratio value between query string and distance
    best_matches_ratio = set([k for k in merged_matches \
        if merged_matches[k][1] == min_ratio])
    if len(best_matches_ratio) == 1:
        return best_matches_ratio
    
    ## If we find more than one best hit, we choose the one with the minimum 
    ## distance
    best_matches_dist = set([k for k in merged_matches \
        if merged_matches[k][0] == min_distance])
    return(best_matches_dist)

def is_unlicensed(lic):
    lic = groom_string_name(lic)
    if lic == 'unlicensed':
        return(True)
    else:
        return(False)


def indentify_links(lic):
    lic = lic.replace('(', '')
    lic = lic.replace(')', '')
    r = re.search(r'http[s]?://.*', lic)
    if r:
        link = r.group(0)
        return(link)
    else:
        return()

## Remove repeated strings
def repeat(input_license):
    
    ## Construct all potential prefixes
    prefix_array = [input_license[:pos] for pos in range(len(input_license))]

    ## Stop at 1st element to avoid checking for the ' ' char
    for subtoken in prefix_array[:1:-1]:
        if input_license.count(subtoken) > 1 :
            #find where the next repetition starts
            offset = input_license[len(subtoken):].find(subtoken)
            return input_license[:len(subtoken)+offset]

    return input_license



## THIS FUNCTION RETURNS LIST OF MATCHES FOR A LICENSE
def match_license(original_license):
    '''
    input: license name to match to SPDX. It must be a string
    '''
    # unlicesed matches well to gnu after grooming (un), it is handled here
    if is_unlicensed(original_license):
        return("unlicensed")

    ## We keep the original input license 
    lic = original_license

    ## Introduce some modifications to match better some common mistakes.
    if lic.lower().find("gpl ") != -1:
        lic = lic.lower().replace("gpl", "General Public License")

    if lic.lower().find("gnu") != -1 and lic.lower().find("gpl,") != -1:
        lic = lic.lower().replace("gpl,", "General Public License ")
    
    ## We remove some unintended repetitions
    lic = repeat(lic)

    matches = {}
    # matching for all tokens to short names
    matches.setdefault("short_names", match_tokens_short_names(lic))

    ## We adjust the threshold as function of the input length of the license
    ## Otherwise, a threshold of 11 for very long names penalize the search
    allowed_distance = 11
    lenght_full_name = len(lic)
    if lenght_full_name > 30 and lenght_full_name < 41:
        allowed_distance = 16
    elif lenght_full_name > 40:
        allowed_distance = 21
    
    # Looking for matches using licenses full name
    matches.setdefault("long_names", match_main_in_name(groom_string_name(lic),
        allowed_distance))

    if not matches["short_names"] and not matches["long_names"]:
        return None
    # compare short and long matches to decide a best overall match
    final_id_matches = compare_short_long(matches['short_names'], \
        matches['long_names'])

    ## we only return one of the equally good matches
    if final_id_matches:
        single_id_to_return = list(final_id_matches)[0]
    else:
        single_id_to_return = None

    # return final matches and links
    return(single_id_to_return)


tests = [
    "GNU General Public v2 or later (GPLv2+)",
    "GNU General Public License v3.0",
    "GNU General Public License v3 (GPLv3)",
    "GPLv2+",
    "GNU General Public License v3 (GPLv3) (see https://www.gnu.org/licenses/gpl-3.0.html)",
    "GNU General Public v2 or later (GPLv2+)",
    "GPL-3 | BSL-1.0 + file LICENSE",
    "Apache-2",
    "GNU Affero General Public License v3 or later (AGPLv3+)",
    "Artistic-2.0-only",
    "Artistic-2.0",
    "Apache License (== 2.0) + file LICENSE",
    "Apache License (== 2.0)",
    "GPL Version 2 or later",
    "Free for Academic Use",
    "UC-LBL license (see package)",
    "GPL (http://www.gnu.org/copyleft/gpl.html)",
    "GPL (version 2 or later)",
    "Affero GPL 3.0",
    "GPL version 2 or newer",
    "GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007",
    "Artistic-2.0Artistic-2.0",
    "GNU-GPL, Version 2.0",
    "GPL, Version 2.0",
]

for test in tests:
    print (("Input\t%s\nOutput\t%s\n") % (test, match_license(test)))
