import requests
import pandas as pd
import json
import Levenshtein
from collections import Counter

"""
Input: https://openebench.bsc.es/monitor/tool
Output: counter of matchs licenses.

"""


def request_given_url(url):
    # Request tools with proper headers, return json.
    headers = {'Accept': 'application/json'}
    with requests.get(url, stream=True, headers=headers) as r:
        return r.json()


def extract_licenses(tools):
    # Extract all licenses from tools and return list of licenses.
    all_licenses = []
    for t in tools:
        try:
            if t['license']:
                all_licenses.append(t['license'])
        except:
            continue
    return all_licenses


def load_json(path_file):
    with open(path_file) as f:
        return json.load(f)


def create_dataframe(data):
    # Create a dataframe to operate and return it
    return pd.DataFrame(data)


def write_json(data, path):
    # Write on a json file:
    with open(path, 'w') as f:
        json.dump(data, f)

def replacement_for_matching(text):
    text = text.replace("(", "")
    text = text.replace(")", "+")
    text = text.replace(">=", "")
    text = text.replace("newer", "+")
    text = text.replace("==", "-")
    text = text.replace("licence", "")
    text = text.replace("license", "")
    text = text.replace("version", "")
    text = text.replace(".0", "")
    text = text.replace("file", "")
    text = text.replace(">", "+")
    text = text.replace(",", "")
    text = text.replace("gpl ", "gpl-")
    return text

def extract_license_by_tokens(item, df, column, min_distance):
    # Compare by tokens
    list_tokens = item.split(" ")
    rate_max = [min_distance, 0]
    # Iterate in unique_licenses of the API:
    for token in list_tokens:
        # Iterate in df_column items of the license.
        for k, item in enumerate(df[column].tolist()):
            item = item.lower()
            try:
                s = Levenshtein.distance(token, item)
            except:
                pass
            if s < rate_max[0]:
                rate_max = [s, k]
    return df.iloc[rate_max[1]].to_dict()

def extract_sequences(df, column, min_distance, licenses_to_operate, unique_licenses):
    # Compare the licenses to a column of the dataframe from github
    final_dict_match = {}
    # Iterate in unique_licenses of the API:
    for i, license_muted in enumerate(licenses_to_operate):
        rate_max = [min_distance, 0]
        #Iterate in df_column and match
        for k, item in enumerate(df[column].tolist()):
            item = item.lower()
            item = item.replace(".0", "")
            s = Levenshtein.distance(license_muted, item)
            if s < rate_max[0]:
                rate_max = [s, k]
        if rate_max[0] < min_distance and license_muted.split(" "):
            final_dict_match[unique_licenses[i]
                             ] = df.iloc[rate_max[1]].to_dict()
        else:
            final_dict_match[unique_licenses[i]] = extract_license_by_tokens(
                license_muted, df, "licenseId", 15)
    return final_dict_match

def last_matching(all_licenses, websites_licenses, levenshtein_final_dict_id):
    final_dict_matches = {}
    # Iterate on all license
    for license in all_licenses:
        # Convert license to lower for matching
        license_lower = license.lower()
        if license_lower in websites_licenses.keys():
            # Check if in websites:
            final_dict_matches[license] = websites_licenses[license_lower]
            continue
        if license_lower in levenshtein_final_dict_id.keys():
            final_dict_matches[license] = levenshtein_final_dict_id[license_lower]
    return final_dict_matches


def extract_counter(final_dict_matches, all_licenses):
    final_list = []
    for t in all_licenses:
        if isinstance(final_dict_matches[t], dict):
            final_list.append([t, final_dict_matches[t]['name']])
        else:
            final_list.append([t, t])
    return Counter([f[1] for f in final_list]).most_common()

def main():
    print("INFO: Starting the request. This might take few seconds.")
    tools = request_given_url("https://openebench.bsc.es/monitor/tool")

    all_licenses = extract_licenses(tools)
    print(f"INFO: {len(all_licenses)} extracted licenses.")

    github = request_given_url(
        "https://raw.githubusercontent.com/spdx/license-list-data/master/json/licenses.json")

    df = create_dataframe(github['licenses'])

    unique_licenses = list(set([a.lower() for a in all_licenses]))
    print(f"INFO: {len(unique_licenses)} unique licenses.")

    websites_licenses = load_json("input_data/website_licenses.json")

    licenses_muted = [replacement_for_matching(a) for a in unique_licenses]

    levenshtein_final_dict_id = extract_sequences(
        df, 'licenseId', 5, licenses_muted, unique_licenses)

    final_dict_matches = last_matching(
        all_licenses, websites_licenses, levenshtein_final_dict_id)

    counter = extract_counter(final_dict_matches, all_licenses)
    print(f"\n\nINFO: Last final counter:\n")
    print(counter)


if __name__ == "__main__":
    main()
