from operator import getitem
from functools import reduce
from collections import abc
from collections import OrderedDict
import re
import json
import requests

"""

Input: https://openebench.bsc.es/monitor/tool
Output: Metrics calculated in JSON file called: metric_a2.1_a2.2._results.json

Problematic tool:
['name'] == 'Arabidopsis Co-expression Tool (ACT)'
[@version] = ['II', '2', '2.0']
As shown the versions are the same but there are 3 entries.

"""

def request_api(url):
    # Request an api with proper headers, return json decoded.
    headers = {'Accept': 'application/json'}
    with requests.get(url, stream=True, headers=headers) as response:
        return response.json()

def group_tools_by_name(iterable):
    # Matching the tools with his corresponding name and extracting only tools that has a version.
    versions_dict = {}
    for tool in iterable:
        #check if the tools is existing
        if tool['@version'] and not tool['@version'] == "none":
            if tool['name'] in versions_dict.keys():
                # check if the version is in the entry of the tool
                if not tool['@version'] in [t['@version'] for t in versions_dict[tool['name']]]:
                    versions_dict[tool['name']].append(tool)
            else:
                versions_dict[tool['name']] = [tool]
    return versions_dict

def delete_item_of_dict(dictionary):
    # Deleting the tools that has only one entry.
    for key in [t[0] for t in dictionary.items() if len(t[1]) == 1]:
        del dictionary[key]
    return dictionary

def get_paths(source):
    # Extract the path of an json file (arrays, dicts...)
    paths = []
    # found a dict-like structure
    if isinstance(source, abc.MutableMapping):
        for key, value in source.items():  # iterate over it
            paths.append([key])  # add the current child path
            # get sub-paths, extend with the current
            paths += [[key] + x for x in get_paths(value)]
    elif isinstance(source, abc.Sequence) and not isinstance(source, str):
        # Append all the path as a array
        for i, value in enumerate(source):
            paths.append([i])
            # get sub-paths, extend with the current
            paths += [[i] + x for x in get_paths(value)]
    return paths

def get_atribute_from_item_with_path(array_path_to_value, item):
    # Return the item from a given path insite of dictionaries, arrays.
    return reduce(getitem, array_path_to_value, item)

def get_puntuation(entry):
    # Calculate the number of paths in the tools and find if there is something or not.
    paths = get_paths(entry)
    counter_true_paths = 0
    for path in paths:
        if get_atribute_from_item_with_path(path, entry):
            counter_true_paths += 1
    return str(f"{counter_true_paths}/{len(paths)}")

def request_website(url):
    # Request website and return code. if it's available.If not return exception catched.
    try:
        with requests.get(url, stream=True) as response:
            return response.status_code
    # catch all the exceptions possible.
    except Exception as excep:
        return str(excep)

def check_if_value_exists(item, path):
    # Check if the value exist inside a path and return it.
    try:
        return get_atribute_from_item_with_path(path, item)
    except KeyError:
        return None

def extract_http_code_metrics(entry):
    # replace the @id to access get the url of metrics
    url_metrics = entry['@id'].replace("/tool/", "/metrics/")
    # get the item of metrics
    tool_metrics = request_api(url_metrics)
    # return the http code of the object
    return get_atribute_from_item_with_path(['project','website','operational'], tool_metrics)

def get_repositories_and_website(entry):
    # inicialise the variables to fill them
    dict_metric_a2 = {}
    dict_metric_a2['same_repo_website'] = []
    dict_metric_a2['repository_codes'] = []
    dict_metric_a2['website_code'] = []
    # Check if the value and return the if exists.
    repositories = check_if_value_exists(entry, ['repositories'])
    website =  check_if_value_exists(entry, ['web', 'homepage'])
    # Extract code from website
    if website:
        dict_metric_a2['website_code'].append(extract_http_code_metrics(entry))
    # iter on repositories and get his http code.
    if repositories:
        for repo in repositories:
            # check if the website is the same as the repositories.
            if website == repo:
                dict_metric_a2['same_repo_website'].append(True)
                code = extract_http_code_metrics(entry)
                dict_metric_a2['repository_codes'].append(code)
            else:
                dict_metric_a2['same_repo_website'].append(False)
                dict_metric_a2['repository_codes'].append(request_website(repo))
    return dict_metric_a2

def extract_metrics(entry):
    # Get all the difference metrics of each version.
    entry_metrics = {}
    entry_metrics['puntuation_A2.1'] = get_puntuation(entry)
    entry_metrics['A2.2'] = get_repositories_and_website(entry)
    return entry_metrics

def atoi(text):
    # Both functions to order a string with numbers and characters:
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [atoi(c) for c in re.split(r'(\d+)', text)]

def calculate_metric(grouped_tools_2_entries_min):
    # From the dictionaries of the tools with more than one entry it calculates the metrics:
    dict_total_versions = {}
    for t in grouped_tools_2_entries_min.items():
        # iterate on the names of tools with has different version
        dict_versions = {}
        for entry in t[1]:
            # for each version calculated the metrics
            dict_versions[entry['@version']] = extract_metrics(entry)
        # order the versions by the number of the versions:
        dict_versions = dict(OrderedDict(
            sorted(dict_versions.items(), key=lambda t: natural_keys(t[0]))))
        dict_total_versions[t[0]] = dict_versions
    return dict_total_versions

def write_json(data, path):
    # Write on a json file givn path
    with open(path, 'w') as f:
        json.dump(data, f)

def main():

    print("INFO: Starting the request. ESTIMATED TIME: 7s.")
    tools = request_api("https://openebench.bsc.es/monitor/tool")

    # Group tools by name. If they have at least one entry with a version.
    grouped_tools_with_1_min_version = group_tools_by_name(tools)

    print(f"INFO: {len(grouped_tools_with_1_min_version)} tools with 1 or more entries with version.")

    # Group tools with at least 2 versions to compare.
    grouped_tools_2_more_entries = delete_item_of_dict(
        grouped_tools_with_1_min_version)
    print(
        f"INFO: {len(grouped_tools_2_more_entries)} tools with 2 or more entries with different versions each entry.")

    print("INFO: Calculating the metrics. ESTIMATED TIME: 15.18min")
    # Calculate metrics
    calculated_metrics_dict = calculate_metric(grouped_tools_2_more_entries)

    # Save metrics in a JSON file
    write_json(calculated_metrics_dict, "metric_a2.1_a2.2._results.json")
    print("INFO: Saved the obtained stadistics in: metric_a2.1_a2.2._results.json")

if __name__ == "__main__":
    main()
